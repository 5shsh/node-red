module.exports = function(RED) {
    var request = require("request");

    function AwsGarbageCollection(config) {
        RED.nodes.createNode(this,config);

        var node = this;
        node.on('input', function(msg) {
            
            var dateToCheck = new Date();
            dateToCheck.setDate(dateToCheck.getDate() + parseInt(config.days));

            var url = "http://service.stuttgart.de/lhs-services/aws/api?street={{{street}}}&streetnr={{{streetnr}}}&datefrom={{{tomorrow.day}}}.{{{tomorrow.month}}}.{{{tomorrow.year}}}&dateto={{{tomorrow.day}}}.{{{tomorrow.month}}}.{{{tomorrow.year}}}";

            url = url
                    .replace(/{{{street}}}/g, encodeURIComponent(config.street))
                    .replace(/{{{streetnr}}}/g, encodeURIComponent(config.streetnr))
                    .replace(/{{{tomorrow.day}}}/g, dateToCheck.getDate())
                    .replace(/{{{tomorrow.month}}}/g, dateToCheck.getMonth() + 1)
                    .replace(/{{{tomorrow.year}}}/g, dateToCheck.getFullYear());
          
            request({
                followAllRedirects: true,
                url: url
            }, function (error, response, body) {
                if (error) {
                    node.send([null, null, null]);
                    return;
                }
                    
                body = JSON.parse(body);
                
                var garbage = body.SERVLET.DIALOG.TERMINELIST.TERMIN;
                
                if (typeof garbage == "undefined") {
                    node.send([null, null, null]);
                    return;
                }

                var info = {
                    "restmuell": null,
                    "biomuell": null,
                    "altpapier": null,
                    "gelbersack": null
                };
                    
                for (var i = 0; i < garbage.length; i++) {
                    console.log(i, garbage[i].FRAKTION );
                    switch (garbage[i].FRAKTION) {
                        case "Restmüll": 
                            if (garbage[i].TURNUS == "02-wöchentl.") info.restmuell = {"payload": garbage[i]};
                            break;
                        case "Biomüll":
                            info.biomuell = {"payload": garbage[i]};
                            break;
                        case "Altpapier":
                            info.altpapier = {"payload": garbage[i]};
                            break;
                        case "Gelber Sack":
                            info.gelbersack = {"payload": garbage[i]};
                            break;
                    }
                }
                    
                node.send([info.restmuell, info.biomuell, info.altpapier, info.gelbersack]);
            });
        });
    }
    RED.nodes.registerType("aws-stuttgart", AwsGarbageCollection);
}