docker_run:
	docker run -d --name=mosquitto_test_run -p 1883:1883 homesmarthome/mosquitto:latest
	docker run -d \
      --name=nodered_test_run \
      -p 1880:1880 \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep nodered_test_run

docker_stop:
	docker rm -f nodered_test_run 2> /dev/null; true
	docker rm -f mosquitto_test_run 2> /dev/null; true