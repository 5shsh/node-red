#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------

ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=node
ARG BASE_IMAGE_VERSION=14.16.0-alpine3.12

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
RUN apk --no-cache add tzdata python3 make g++

RUN npm install -g --unsafe-perm node-red   
RUN npm install -g --unsafe-perm node-red-contrib-slack 
RUN npm install -g --unsafe-perm node-red-contrib-bigtimer 
RUN npm install -g --unsafe-perm node-red-contrib-timecheck 
RUN npm install -g --unsafe-perm node-red-dashboard
RUN npm install -g --unsafe-perm node-red-contrib-telegrambot
RUN npm install -g --unsafe-perm node-red-contrib-simple-gate
RUN npm install -g --unsafe-perm node-red-contrib-homeconnect
RUN npm install -g --unsafe-perm mustache
RUN npm install -g --unsafe-perm node-red-contrib-map
RUN rm -rf /usr/local/lib/node_modules/node-red/nodes/core/analysis/

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
RUN apk --no-cache add tzdata docker

COPY --from=builder /usr/local/bin/node /usr/local/bin/node
COPY --from=builder /usr/local/lib/node_modules /usr/local/lib/node_modules

RUN ln -s /usr/local/lib/node_modules/node-red/red.js /usr/local/bin/node-red && \
    ln -s /usr/local/lib/node_modules/node-red/node-red-pi /usr/local/bin/node-red-pi

COPY ./image_files /

COPY ./node-red-contrib-eztimer /usr/local/lib/node_modules/node-red-contrib-eztimer 
RUN cd /root/.node-red/ && npm install /usr/local/lib/node_modules/node-red-contrib-eztimer

ENV TZ Europe/Berlin
RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/timezone

EXPOSE 1880

CMD ["node-red"]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#-------------------------------------------------------------------------------
# Labelling
#-------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/node-red" \
      de.5square.homesmarthome.description="Node-Red for flow control (Concierge) in HomeSmartHome environment" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
