# Node-Red #

[![](https://images.microbadger.com/badges/image/homesmarthome/node-red.svg)](https://microbadger.com/images/homesmarthome/node-red)

[![](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%2Fnode-red%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4)]("https://hub.docker.com/r/homesmarthome/node-red/")

## Install as a Service with Portainer
1. Open portainer ([homesmarthome.local:9000](http://homesmarthome.local:9000)
2. Click on *Services* in the menu
3. Click on *+ Add Service*
4. Configuration:

    * Name: `node-red`
    * Image configuration
        * Name: ![](https://img.shields.io/badge/dynamic/json.svg?label=Image&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%2Fnode-red%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4&prefix=homesmarthome/node-red:&style=flat-square)
    * Ports configuration
        * Port mapping (`+map additional port`)
            * host: `1880`
            * container: `1880`
    * Volumes (map additinal volume)
        * container: `/var/run/docker.sock` (`Bind`)
        * host: `/var/run/docker.sock`
    * Volumes (map additinal volume)
        * container: `/config` (`Bind`)
        * host: `/config/node-red-config`
    * Network
        * Network: `hsh-net`
    * Resources & Placement
        * Placement constraints (`+placement constraint`)
            * name: `node.role`
            * value: `manager`
            
5. Actions
    * `Create the service`

## Docker Swarm service
```
docker service create \
    --name=node-red \
    --publish=1880:1880/tcp \
    --constraint=node.role==manager \
    --mount=type=bind,src=/config/node-red-config,dst=/config \
    --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
    --network=hsh-net \
    homesmarthome/node-red:latest
```

### config
Folder ```/config/node-red-config``` has to be writable by node-red service. File ```flows.json``` is being stored in this directory.